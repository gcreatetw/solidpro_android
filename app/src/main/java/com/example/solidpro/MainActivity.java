package com.example.solidpro;

import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothHeadset;
import android.bluetooth.BluetoothProfile;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.hardware.usb.UsbDevice;
import android.hardware.usb.UsbManager;
import android.net.wifi.WifiConfiguration;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.storage.StorageManager;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentManager;

import com.example.solidpro.View.F_main;

import java.lang.reflect.Array;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

public class MainActivity extends AppCompatActivity {

    private static final String TAG = "MainActivity.class";
    private TextView tv_current_date, tv_current_time;
    private Handler handler = new Handler();
    //----------- WIFI var ------------
    private WifiInfo wifiInfo = null;               //獲得的Wifi信息
    private WifiManager wifiManager = null;        //Wifi管理器
    private ImageView wifiImage;                //信號圖片顯示
    //----------- USB var ------------
    private ImageView usbImage;
    private ImageView bluetoothImage;
    private TextView hotspot;
    private ImageView sdcardImage;
    //---------- Installed Apps ----------
    private ImageView installedApps;
    private UsbManager usbManager;
    private HashMap<String, UsbDevice> deviceList;
    private F_main f_main = new F_main();
    private LinearLayout ly_hospot;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        findViews();
        initIntentFilter();
        initStatusBarItem();
        getApSSID();
        isWifiApEnabled();

        global.getWindowSize(this);
        new global().replaceFragment(this, f_main);


    }

    public static String getStoragePath(Context mContext, String keyword) {
        String targetpath = "";
        StorageManager mStorageManager = (StorageManager) mContext.getSystemService(Context.STORAGE_SERVICE);
        Class<?> storageVolumeClazz = null;
        try {
            storageVolumeClazz = Class.forName("android.os.storage.StorageVolume");

            Method getVolumeList = mStorageManager.getClass().getMethod("getVolumeList");

            Method getPath = storageVolumeClazz.getMethod("getPath");

            Object result = getVolumeList.invoke(mStorageManager);

            final int length = Array.getLength(result);

            Method getUserLabel = storageVolumeClazz.getMethod("getUserLabel");


            for (int i = 0; i < length; i++) {

                Object storageVolumeElement = Array.get(result, i);

                String userLabel = (String) getUserLabel.invoke(storageVolumeElement);

                String path = (String) getPath.invoke(storageVolumeElement);

                if (userLabel.contains(keyword)) {
                    targetpath = path;
                }
            }
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }

        return targetpath;
    }

//    public Integer getPrimaryStoragePath() {
//
//        try {
//            StorageManager sm = (StorageManager) getSystemService(STORAGE_SERVICE);
//            Method getVolumePathsMethod = StorageManager.class.getMethod("getVolumeList", null);
//            String[] paths = (String[]) getVolumePathsMethod.invoke(sm, null);
//            List<String> List = (java.util.List<String>) getVolumePathsMethod.invoke(sm, null);
//            // first element in paths[] is primary storage path
//            List<String> temp = new ArrayList<String>();
//            for (int i = 0; i < List.size(); i++) {
//                List.add("Path" + i + List.get(i));
//            }
//            textView.setText(List.size() + "" + List);
//            //textView.setText(paths[0]);
//            return paths.length;
//        } catch (Exception e) {
//            Log.e(TAG, "getPrimaryStoragePath() failed", e);
//        }
//        return null;
//    }

    private void findViews() {
        tv_current_date = findViewById(R.id.tv_main_date);
        tv_current_time = findViewById(R.id.tv_main_time);
        wifiImage = findViewById(R.id.img_main_wifiIcon);
        wifiManager = (WifiManager) getSystemService(WIFI_SERVICE);
        hotspot = findViewById(R.id.tv_hotspot);
        usbImage = findViewById(R.id.img_main_usbIcon);
        bluetoothImage = findViewById(R.id.img_main_blueToothIcon);
        installedApps = findViewById((R.id.img_appCenter));
        sdcardImage = findViewById(R.id.img_main_sdcardIcon);
        ly_hospot = findViewById(R.id.ly_hotspot);
    }

    private void initIntentFilter() {
        IntentFilter usbDeviceStateFilter = new IntentFilter();
        usbDeviceStateFilter.addAction(UsbManager.ACTION_USB_DEVICE_ATTACHED);
        usbDeviceStateFilter.addAction(UsbManager.ACTION_USB_DEVICE_DETACHED);
        registerReceiver(usbReceiver, usbDeviceStateFilter);

        IntentFilter bluetoothStatusFilter = new IntentFilter();
        bluetoothStatusFilter.addAction(BluetoothDevice.ACTION_ACL_CONNECTED);
        bluetoothStatusFilter.addAction(BluetoothDevice.ACTION_ACL_DISCONNECTED);
        bluetoothStatusFilter.addAction(BluetoothAdapter.ACTION_STATE_CHANGED);
        registerReceiver(bluetoothReceiver, bluetoothStatusFilter);

        IntentFilter wififilter = new IntentFilter();
        wififilter.addAction("android.net.conn.CONNECTIVITY_CHANGE");
        wififilter.addAction("android.net.wifi.WIFI_STATE_CHANGED");
        wififilter.addAction("android.net.wifi.STATE_CHANGE");
        wififilter.addAction(WifiManager.RSSI_CHANGED_ACTION);
        wififilter.addAction(WifiManager.WIFI_STATE_CHANGED_ACTION);
        registerReceiver(wifiReceiver, wififilter);

        IntentFilter sdcardStatusFilter = new IntentFilter();
        sdcardStatusFilter.addAction(Intent.ACTION_MEDIA_MOUNTED);
        sdcardStatusFilter.addAction(Intent.ACTION_MEDIA_UNMOUNTED);
        sdcardStatusFilter.addDataScheme("file");
        registerReceiver(sdcardReceiver, sdcardStatusFilter);
    }

    private void initStatusBarItem() {
        //--- time check ---
        routineUpdateTime();
        //--- wifi check ---
        if (wifiManager.isWifiEnabled())
            wifiImage.setVisibility(View.VISIBLE);
        //--- usb check ---0.03
        usbManager = (UsbManager) getSystemService(Context.USB_SERVICE);
        deviceList = usbManager.getDeviceList();
        if (deviceList.values().size() != 0) {
            usbImage.setVisibility(View.VISIBLE);
        }
        //--- bluetooth check ---
        BluetoothAdapter bluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        int flag = 0;
        int a2dp = bluetoothAdapter.getProfileConnectionState(BluetoothProfile.A2DP);
        int headset = bluetoothAdapter.getProfileConnectionState(BluetoothProfile.HEADSET);
        int health = bluetoothAdapter.getProfileConnectionState(BluetoothProfile.HEALTH);

        if (a2dp == BluetoothProfile.STATE_CONNECTED) {
            flag = a2dp;
        } else if (headset == BluetoothProfile.STATE_CONNECTED) {
            flag = headset;
        } else if (health == BluetoothProfile.STATE_CONNECTED) {
            flag = health;
        }

        if (flag != -1) {
            bluetoothAdapter.getProfileProxy(MainActivity.this, new BluetoothProfile.ServiceListener() {
                @Override
                public void onServiceDisconnected(int profile) {
                    bluetoothImage.setVisibility(View.GONE);
                }
                @Override
                public void onServiceConnected(int profile, BluetoothProfile proxy) {
                    bluetoothImage.setVisibility(View.VISIBLE);
                }
            }, flag);
        }

        //--- sdcard check ---
        if (getStoragePath(this, "SD") != "")
            sdcardImage.setVisibility(View.VISIBLE);

        //--- hotspot check ---
        if (isWifiApEnabled()) {
            ly_hospot.setVisibility(View.VISIBLE);
            hotspot.setText(getApSSID() + "/" + getApSSIDAndPwd(this));
        } else {
            ly_hospot.setVisibility(View.GONE);
        }
    }

    //-------------------------- Update current time --------------------------//
    private void routineUpdateTime() {
        Runnable time = new Runnable() {
            public void run() {
                handler.postDelayed(this, 1000);
                SimpleDateFormat formatter_1 = new SimpleDateFormat("yyyy/MM/dd", Locale.ENGLISH);
                SimpleDateFormat formatter_2 = new SimpleDateFormat("a hh:mm ", Locale.ENGLISH);
                Date curDate = new Date(System.currentTimeMillis());
                tv_current_date.setText(formatter_1.format(curDate));
                tv_current_time.setText(formatter_2.format(curDate));
            }
        };
        handler.post(time);
    }

    //-------------------------- 監聽 SD Receiver  --------------------------//
    public BroadcastReceiver sdcardReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            switch (action) {
                case Intent.ACTION_MEDIA_UNMOUNTED:
                    if (getStoragePath(context, "SD").equals(""))
                        sdcardImage.setVisibility(View.GONE);
                    break;
                case Intent.ACTION_MEDIA_MOUNTED:
                    if (!getStoragePath(context, "SD").equals(""))
                        sdcardImage.setVisibility(View.VISIBLE);
                    break;
                default:
                    break;
            }
        }
    };

    //-------------------------- 監聽 Wifi 信號強弱 --------------------------//
    private int obtainWifiInfo() {
        // Wifi的連線速度及訊號強度:
        int strength = 0;
        WifiManager wifiManager = (WifiManager) getSystemService(WIFI_SERVICE);
        WifiInfo info = wifiManager.getConnectionInfo();
        if (info.getBSSID() != null) {
            // 連結訊號強度,5為獲取的訊號強度值在5以內
            strength = WifiManager.calculateSignalLevel(info.getRssi(), 5);
            // 連結速度
            int speed = info.getLinkSpeed();
            // 連結速度單位
            String units = WifiInfo.LINK_SPEED_UNITS;
            // Wifi源名稱
            String ssid = info.getSSID();
        }
        return strength;
    }

    public BroadcastReceiver wifiReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (WifiManager.WIFI_STATE_CHANGED_ACTION.equals(intent.getAction())) {
                int wifiState = intent.getIntExtra(WifiManager.EXTRA_WIFI_STATE, 0);
                switch (wifiState) {
                    case WifiManager.WIFI_STATE_DISABLED:
                        wifiImage.setVisibility(View.GONE);
                        break;
                    case WifiManager.WIFI_STATE_ENABLED:
                        wifiImage.setVisibility(View.VISIBLE);
                        int s = obtainWifiInfo();
                        switch (s) {
                            case 4:
                                wifiImage.setImageResource(R.mipmap.icon_other_wifi_100);
                                break;
                            case 3:
                                wifiImage.setImageResource(R.mipmap.icon_other_wifi_66);
                                break;
                            case 2:
                                wifiImage.setImageResource(R.mipmap.icon_other_wifi_33);
                                break;
                            default:
                                wifiImage.setVisibility(View.VISIBLE);
                        }
                        break;
                    default:
                        int ss = obtainWifiInfo();
                        switch (ss) {
                            case 4:
                                wifiImage.setImageResource(R.mipmap.icon_other_wifi_100);
                                break;
                            case 3:
                                wifiImage.setImageResource(R.mipmap.icon_other_wifi_66);
                                break;
                            case 2:
                                wifiImage.setImageResource(R.mipmap.icon_other_wifi_33);
                                break;
                            default:
                                wifiImage.setVisibility(View.GONE);
                        }
                        break;
                }
            }
        }
    };

    //-------------------------- 監聽 Bluetooth --------------------------//
    public BroadcastReceiver bluetoothReceiver = new BroadcastReceiver() {
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            switch (action) {
                case BluetoothDevice.ACTION_ACL_CONNECTED:
                    bluetoothImage.setVisibility(View.VISIBLE);
                    break;
                case BluetoothDevice.ACTION_ACL_DISCONNECTED:
                    bluetoothImage.setVisibility(View.GONE);
                    break;
                case BluetoothAdapter.ACTION_STATE_CHANGED:
                    int blueState = intent.getIntExtra(BluetoothAdapter.EXTRA_STATE, 0);
                    switch (blueState) {
                        case BluetoothAdapter.STATE_OFF:
                            bluetoothImage.setVisibility(View.GONE);
                            break;
                    }
                    break;
                default:
                    break;
            }
        }
    };

    //-------------------------- 監聽 USB --------------------------//
    public BroadcastReceiver usbReceiver = new BroadcastReceiver() {
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            if (UsbManager.ACTION_USB_DEVICE_DETACHED.equals(action)) {
                usbImage.setVisibility(View.GONE);
            } else if (UsbManager.ACTION_USB_DEVICE_ATTACHED.equals(action)) {
                usbImage.setVisibility(View.VISIBLE);
            }
        }
    };

    @Override
    public void onBackPressed() {
        FragmentManager fragmentManager = getSupportFragmentManager();
        if (fragmentManager.getBackStackEntryCount() > 1) {
            fragmentManager.popBackStackImmediate();

        } else {
            finish();
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unregisterReceiver(wifiReceiver);
        unregisterReceiver(bluetoothReceiver);
        unregisterReceiver(sdcardReceiver);
        unregisterReceiver(usbReceiver);
    }

    //----------------------------------- WIFI 熱點名稱-------------------------------------------------------//
    public String getApSSID() {
        try {
            Method localMethod = this.wifiManager.getClass().getDeclaredMethod("getWifiApConfiguration");
            if (localMethod == null) return null;
            Object localObject1 = localMethod.invoke(this.wifiManager);
            if (localObject1 == null) return null;
            WifiConfiguration localWifiConfiguration = (WifiConfiguration) localObject1;
            if (localWifiConfiguration.SSID != null) return localWifiConfiguration.SSID;
            Field localField1 = WifiConfiguration.class.getDeclaredField("mWifiApProfile");
            if (localField1 == null) return null;
            localField1.setAccessible(true);
            Object localObject2 = localField1.get(localWifiConfiguration);
            localField1.setAccessible(false);
            if (localObject2 == null) return null;
            Field localField2 = localObject2.getClass().getDeclaredField("SSID");
            localField2.setAccessible(true);
            Object localObject3 = localField2.get(localObject2);
            if (localObject3 == null) return null;
            localField2.setAccessible(false);
            String str = (String) localObject3;
            return str;
        } catch (Exception localException) {
        }
        return null;
    }

    public boolean isWifiApEnabled() {
        try {
            Method method = wifiManager.getClass().getMethod("isWifiApEnabled");
            method.setAccessible(true);
            return (Boolean) method.invoke(wifiManager);
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    public static WifiConfiguration getWifiConfiguration(Context context) {
        WifiConfiguration mWifiConfig = null;
        try {
            WifiManager wifiManager = (WifiManager) context.getSystemService(WIFI_SERVICE);
            Method method = wifiManager.getClass().getMethod("getWifiApConfiguration");
            method.setAccessible(true);
            mWifiConfig = (WifiConfiguration) method.invoke(wifiManager);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return mWifiConfig;
    }

    public static String getApSSIDAndPwd(Context context) {
        WifiConfiguration mWifiConfig = getWifiConfiguration(context);
        String ssid = null;
        String pwd = null;
        if (null != mWifiConfig) {
            Field[] fields = mWifiConfig.getClass().getDeclaredFields();
            if (null != fields) {
                for (Field field : fields) {
                    try {
                        if (field.getName().equals("SSID")) {
                            ssid = field.get(mWifiConfig).toString();
                            Log.e(TAG, "AP SSI = " + ssid);
                        } else if (field.getName().equals("preSharedKey")) {
                            pwd = field.get(mWifiConfig).toString();
                            Log.e(TAG, "AP pwd = " + pwd);
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                        Log.e(TAG, "getApSSIDAndPwd()-->error:" + e);
                    }
                }
            }
        }
        if (null == pwd)
            pwd = "Unknown";
        return pwd;
    }

}
