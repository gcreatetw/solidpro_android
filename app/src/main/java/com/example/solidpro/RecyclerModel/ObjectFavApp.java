package com.example.solidpro.RecyclerModel;

public class ObjectFavApp {

    private String favAppName;
    private String favAppPkgAddress;
    private byte[] bytes;


    public ObjectFavApp(byte[] bytes, String favAppPkgAddress, String favAppName) {
        this.bytes = bytes;
        this.favAppPkgAddress = favAppPkgAddress;
        this.favAppName = favAppName;
    }

    public String getFavAppPkgAddress() {
        return favAppPkgAddress;
    }

    public void setFavAppPkgAddress(String favAppPkgAddress) {
        this.favAppPkgAddress = favAppPkgAddress;
    }

    public byte[] getBytes() {
        return bytes;
    }

    public void setBytes(byte[] bytes) {
        this.bytes = bytes;
    }

    public String getFavAppName() {
        return favAppName;
    }

    public void setFavAppName(String favAppName) {
        this.favAppName = favAppName;
    }
}
