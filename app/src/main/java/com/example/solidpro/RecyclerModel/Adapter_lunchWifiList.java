package com.example.solidpro.RecyclerModel;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.example.solidpro.R;
import com.example.solidpro.global;

import java.util.List;


public class Adapter_lunchWifiList extends RecyclerView.Adapter<Adapter_lunchWifiList.ViewHolder> {

    private List<ObjectLunchWifiItem> list;
    private OnItemClickListener onItemClickListener = null;

    public Adapter_lunchWifiList(List<ObjectLunchWifiItem> list) {
        this.list = list;
    }

    public interface OnItemClickListener {
        void onItemClick(View view, int position);
    }


    public void setOnItemClickListener(OnItemClickListener listener) {
        onItemClickListener = listener;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.card_lunchwifiitem, parent, false);
        v.setFocusable(true);
        v.setFocusableInTouchMode(true);
        return new Adapter_lunchWifiList.ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, final int position) {
        holder.itemView.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus) {
                    holder.cl_lunchWifiItem.setBackground(ContextCompat.getDrawable(global.context, R.drawable.bg_focus_solid_black_radius10));

                } else {
                    holder.cl_lunchWifiItem.setBackground(ContextCompat.getDrawable(global.context, R.drawable.bg_solid_black_radius10));
                }
            }
        });

        holder.tv_wifiName.setText(list.get(position).getItemName());
        if (list.get(position).getNeedpwd()) {
            holder.img_lunchWifiLockIcon.setVisibility(View.VISIBLE);
        } else {
            holder.img_lunchWifiLockIcon.setVisibility(View.INVISIBLE);
        }

        switch (list.get(position).getSignalLevel()) {
            case 1:
                holder.img_lunchWifiIcon.setImageResource(R.mipmap.icon_other_wifi_33);
                break;
            case 2:
                holder.img_lunchWifiIcon.setImageResource(R.mipmap.icon_other_wifi_66);
                break;
            case 3:
                holder.img_lunchWifiIcon.setImageResource(R.mipmap.icon_other_wifi_100);
                break;
        }
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    onItemClickListener.onItemClick(holder.itemView, position);
                } catch (Exception e) {
                    Log.d("Error", "onClick: " + e);
                }
            }
        });

    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        private TextView tv_wifiName;
        private ImageView img_lunchWifiLockIcon, img_lunchWifiIcon;
        private ConstraintLayout cl_lunchWifiItem;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            this.tv_wifiName = itemView.findViewById(R.id.tv_lunchWifiName);
            this.img_lunchWifiLockIcon = itemView.findViewById(R.id.img_lunchWifiLockIcon);
            this.img_lunchWifiIcon = itemView.findViewById(R.id.img_lunchWifiIcon);
            this.cl_lunchWifiItem = itemView.findViewById(R.id.cl_lunchWifiItem);
        }
    }
}
