package com.example.solidpro.RecyclerModel;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.PixelFormat;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.example.solidpro.R;
import com.example.solidpro.global;

import java.util.List;

public class Adapter_InstalledApps extends RecyclerView.Adapter<Adapter_InstalledApps.ViewHolder> {
    Context context;
    private List<ObjectInstalledAppsList> lists;
    private OnItemClickListener onItemClickListener = null;

    public Adapter_InstalledApps(List<ObjectInstalledAppsList> lists) {
        this.lists = lists;
    }


    public interface OnItemClickListener {
        void onItemClick(View view, int position);
    }

    public void setOnItemClickListener(OnItemClickListener listener) {
        onItemClickListener = listener;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        context = parent.getContext();
        View v = LayoutInflater.from(context).inflate(R.layout.card_installedapps, parent, false);
        v.getLayoutParams().width = (int) (((global.windowWidth * 0.9) / 5));
        v.setFocusable(true);
        v.setFocusableInTouchMode(true);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, final int position) {
        holder.itemView.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus) {
                    holder.cardView_installApp.setBackground(ContextCompat.getDrawable(global.context, R.drawable.bg_focus_installapp));
                    holder.cardView_installApp.setElevation(0);
                } else {
                    holder.cardView_installApp.setBackground(ContextCompat.getDrawable(global.context, R.color.white80ffffff));
                }
            }
        });
        final ObjectInstalledAppsList list = lists.get(position);
        Glide.with(holder.itemView).load(list.getAppImg()).into(holder.appImg);
        holder.appName.setText(list.getAppName());
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (global.isComeFormFav) {
                    global.favSelectedImg = list.getAppImg();
                    global.favSelectedAppPkgAddr = list.getPackageName();
                    global.favSelectedAppName = list.getAppName();
                    drawableToBitmap(list.getAppImg());
                    onItemClickListener.onItemClick(holder.itemView, position);
                } else {
                    onItemClickListener.onItemClick(holder.itemView, position);
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        if (lists == null) {
            return 0;
        } else {
            return lists.size();
        }
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        private final TextView appName;
        private final ImageView appImg;
        private ConstraintLayout cl_installapp;
        private CardView cardView_installApp;

        public ViewHolder(View itemiew) {
            super(itemiew);
            appName = itemiew.findViewById(R.id.appName);
            appImg = itemiew.findViewById(R.id.appImg);
            this.cl_installapp = itemView.findViewById(R.id.cl_installapp);
            this.cardView_installApp = itemView.findViewById(R.id.cardView_installApp);
        }
    }

    public static void drawableToBitmap(Drawable drawable) {
        Bitmap bitmap = Bitmap.createBitmap(drawable.getIntrinsicWidth(), drawable.getIntrinsicHeight(),
                drawable.getOpacity() != PixelFormat.OPAQUE ? Bitmap.Config.ARGB_8888 : Bitmap.Config.RGB_565);
        Canvas canvas = new Canvas(bitmap);
        drawable.setBounds(0, 0, drawable.getIntrinsicWidth(), drawable.getIntrinsicHeight());
        drawable.draw(canvas);
    }

}
