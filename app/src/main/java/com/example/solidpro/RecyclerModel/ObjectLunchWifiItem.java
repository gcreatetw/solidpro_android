package com.example.solidpro.RecyclerModel;

public class ObjectLunchWifiItem {
    private String itemName;
    private Boolean isNeedpwd;
    private int signalLevel;

    public ObjectLunchWifiItem(String itemName, Boolean isNeedpwd, int signalLevel) {
        this.itemName = itemName;
        this.isNeedpwd = isNeedpwd;
        this.signalLevel = signalLevel;
    }


    public String getItemName() {
        return itemName;
    }

    public void setItemName(String itemName) {
        this.itemName = itemName;
    }

    public Boolean getNeedpwd() {
        return isNeedpwd;
    }

    public void setNeedpwd(Boolean needpwd) {
        isNeedpwd = needpwd;
    }

    public int getSignalLevel() {
        return signalLevel;
    }

    public void setSignalLevel(int signalLevel) {
        this.signalLevel = signalLevel;
    }
}
