package com.example.solidpro.RecyclerModel;

import android.graphics.drawable.Drawable;

public class ObjectInstalledAppsList {

    private String appName;
    private String packageName;
    private Drawable appImg;

    public ObjectInstalledAppsList() {
    }

    public ObjectInstalledAppsList(Drawable appImg, String appName) {
        this.appName = appName;
        this.appImg = appImg;
    }


    public ObjectInstalledAppsList(String appName, Drawable appImg, String packageName) {
        this.appName = appName;
        this.appImg = appImg;
        this.packageName = packageName;
    }


    public String getAppName() {
        return appName;
    }

    public Drawable getAppImg() {
        return appImg;
    }

    public void setAppName(String appName) {
        this.appName = appName;
    }

    public void setAppImg(Drawable appImg) {
        this.appImg = appImg;
    }

    public String getPackageName() {
        return packageName;
    }

    public void setPackageName(String packageName) {
        this.packageName = packageName;
    }
}
