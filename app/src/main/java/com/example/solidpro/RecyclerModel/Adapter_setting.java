package com.example.solidpro.RecyclerModel;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.example.solidpro.R;
import com.example.solidpro.global;

import java.util.List;
import java.util.Map;

import static com.example.solidpro.global.context;

public class Adapter_setting extends RecyclerView.Adapter<Adapter_setting.ViewHolder> {

    private Map<Integer, List<String>> map;
    private OnItemClickListener onItemClickListener = null;

    public Adapter_setting(Map<Integer, List<String>> map) {
        this.map = map;
    }

    public interface OnItemClickListener {
        void onItemClick(View view, int position);
    }


    public void setOnItemClickListener(OnItemClickListener listener) {
        onItemClickListener = listener;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.card_setting, parent, false);
        v.setFocusable(true);
        v.setFocusableInTouchMode(true);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, final int position) {

        holder.itemView.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus) {
                    holder.cl_setting.setBackground(ContextCompat.getDrawable(global.context, R.drawable.bg_focus_tools));
                    holder.cl_setting.setScaleX((float) 1.3);
                    holder.cl_setting.setScaleY((float) 1.3);
                    holder.settingImg.setBackground(null);
                    holder.settingImg.setColorFilter(ContextCompat.getColor(context, R.color.blue0080CB));
                    holder.settingName.setTextColor(ContextCompat.getColor(context, R.color.blue0080CB));

                } else {
                    holder.cl_setting.setBackground(null);
                    holder.cl_setting.setScaleX((float) 1);
                    holder.cl_setting.setScaleY((float) 1);
                    holder.cl_setting.setBackground(ContextCompat.getDrawable(global.context, R.drawable.bg_solid_black_radius10));
                    holder.settingImg.setColorFilter(ContextCompat.getColor(context, R.color.whiteffffff));
                    holder.settingName.setTextColor(ContextCompat.getColor(context, R.color.whiteffffff));
                }
            }
        });

        holder.settingImg.setImageResource(Integer.parseInt(map.get(position).get(0)));
        holder.settingName.setText(map.get(position).get(1));

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    onItemClickListener.onItemClick(holder.itemView, position);
                } catch (Exception e) {
                    Log.d("Error", "onClick: " + e);
                }
            }
        });

    }

    @Override
    public int getItemCount() {
        return map.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        private ImageView settingImg;
        private TextView settingName;
        private ConstraintLayout cl_setting;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            settingImg = itemView.findViewById(R.id.img_settingImg);
            settingName = itemView.findViewById(R.id.tv_settingName);
            cl_setting = itemView.findViewById(R.id.cl_setting);
        }
    }
}
