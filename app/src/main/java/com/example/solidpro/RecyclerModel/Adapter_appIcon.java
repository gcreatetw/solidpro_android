package com.example.solidpro.RecyclerModel;

import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.example.solidpro.R;
import com.example.solidpro.global;

import java.util.List;

public class Adapter_appIcon extends RecyclerView.Adapter<Adapter_appIcon.ViewHolder> {

    private List<ObjectFavApp> favAppList;
    private OnItemClickListener onItemClickListener = null;
    private OnLongClickListener onLongClickListener = null;
    private long lastClickTime = 0L;
    private static final int FAST_CLICK_DELAY_TIME = 1000;


    public Adapter_appIcon(List<ObjectFavApp> favAppList) {
        this.favAppList = favAppList;
    }


    public interface OnItemClickListener {
        void onItemClick(View view, int position);
    }

    public interface OnLongClickListener {
        void onLongClick(View view, int position);
    }

    public void setOnItemClickListener(OnItemClickListener listener) {
        onItemClickListener = listener;
    }

    public void setOnLongClickListener(OnLongClickListener listener) {
        onLongClickListener = listener;
    }

    @NonNull
    @Override
    public Adapter_appIcon.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.card_favapps, parent, false);
        v.setFocusable(true);
        v.setFocusableInTouchMode(true);
        v.getLayoutParams().width = (int) (((global.windowWidth * 0.52) / 3) * 0.85);
        v.getLayoutParams().height = (int) (((global.windowWidth * 0.52) / 3) * 0.85);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull final Adapter_appIcon.ViewHolder holder, final int position) {

        holder.itemView.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus) {
                    holder.cardView_favIconApp.setBackground(ContextCompat.getDrawable(global.context, R.drawable.bg_focus_favapp));
                    holder.cardView_favIconApp.setElevation(0);
                    holder.cardView_favIconApp.setScaleX((float) 1.1);
                    holder.cardView_favIconApp.setScaleY((float) 1.1);
                } else {
                    holder.cardView_favIconApp.setBackground(ContextCompat.getDrawable(global.context, R.drawable.bg_focus_favapp2));
                    holder.cardView_favIconApp.setScaleX((float) 1);
                    holder.cardView_favIconApp.setScaleY((float) 1);
                }
            }
        });

        if (favAppList.get(position).getFavAppName().equals("")) {
            holder.iconAppName.setVisibility(View.GONE);
            holder.iconImage.getLayoutParams().width = (int) (((global.windowWidth * 0.45) / 3));
            holder.iconImage.getLayoutParams().height = (int) (((global.windowWidth * 0.45) / 3));
            holder.cl_favIconApp.setPadding(0, 0, 0, 0);
            holder.cardView_favIconApp.setRadius(20);
            holder.iconImage.setMaxHeight((int) (((global.windowWidth * 0.52) / 3) * 0.8));
        } else {
            holder.cardView_favIconApp.setCardBackgroundColor(ContextCompat.getColor(global.context, R.color.white20ffffff));
            holder.cardView_favIconApp.setElevation(0);
            holder.cardView_favIconApp.setRadius(20);
            holder.iconAppName.setVisibility(View.VISIBLE);
            holder.iconAppName.setText(favAppList.get(position).getFavAppName());
        }
        holder.iconImage.setImageDrawable(new BitmapDrawable(BitmapFactory.decodeByteArray(favAppList.get(position).getBytes(), 0, favAppList.get(position).getBytes().length)));
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    if (System.currentTimeMillis() - lastClickTime >= FAST_CLICK_DELAY_TIME) {
                        //下面进行其他操作，比如跳转等
                        lastClickTime = System.currentTimeMillis();
                        onItemClickListener.onItemClick(holder.itemView, position);
                    }
                } catch (Exception e) {
                    Log.d("Error", "onClick: " + e);
                }
            }
        });
        holder.itemView.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                try {

                    onLongClickListener.onLongClick(holder.itemView, position);

                } catch (Exception e) {
                    Log.d("Error", "onClick: " + e);
                }
                return true;
            }
        });


    }

    @Override
    public int getItemCount() {
        return favAppList.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        private ImageView iconImage;
        private TextView iconAppName;
        private ConstraintLayout cl_favIconApp;
        private CardView cardView_favIconApp;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            this.iconImage = itemView.findViewById(R.id.img_favAppImg);
            this.iconAppName = itemView.findViewById(R.id.tv_favAppName);
            this.cl_favIconApp = itemView.findViewById(R.id.cl_favApp);
            this.cardView_favIconApp = itemView.findViewById(R.id.cardView_favApp);
        }
    }
}
