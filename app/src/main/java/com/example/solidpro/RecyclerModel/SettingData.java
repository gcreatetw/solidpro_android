package com.example.solidpro.RecyclerModel;

import com.example.solidpro.R;

import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

public class SettingData {


    public Map<Integer, List<String>> getWifiSettingData() {
        Map<Integer, List<String>> map = new LinkedHashMap<>();
        map.put(0, Arrays.asList(R.mipmap.icon_other_wifi_100 + "", "WIFI"));
        map.put(1, Arrays.asList(R.mipmap.icon_other_bluetooch + "", "藍牙"));
        map.put(2, Arrays.asList(R.mipmap.icon_wireless_projection + "", "無線熱點"));

        return map;
    }

    public Map<Integer, List<String>> getProjectionSettingData() {
        Map<Integer, List<String>> map = new LinkedHashMap<>();
        map.put(0, Arrays.asList(R.mipmap.network + "", "畫面模式"));
        map.put(1, Arrays.asList(R.mipmap.network + "", "梯形校正"));
        map.put(2, Arrays.asList(R.mipmap.network + "", "投影方式"));
        map.put(3, Arrays.asList(R.mipmap.network + "", "變焦"));
        map.put(4, Arrays.asList(R.mipmap.network + "", "自動對焦"));
        return map;
    }

    public Map<Integer, List<String>> getSoundSettingData() {
        Map<Integer, List<String>> map = new LinkedHashMap<>();
        map.put(0, Arrays.asList(R.mipmap.network + "", "音效"));
        map.put(1, Arrays.asList(R.mipmap.network + "", "按鍵音"));
        map.put(2, Arrays.asList(R.mipmap.network + "", "靜音"));
        map.put(3, Arrays.asList(R.mipmap.network + "", "音響模式"));
        return map;
    }

    public Map<Integer, List<String>> getGeneralSettingData() {
        Map<Integer, List<String>> map = new LinkedHashMap<>();
        map.put(0, Arrays.asList(R.mipmap.network + "", "語言"));
        map.put(1, Arrays.asList(R.mipmap.network + "", "輸入法"));
        map.put(2, Arrays.asList(R.mipmap.network + "", "主題"));
        map.put(3, Arrays.asList(R.mipmap.network + "", "時間設置"));
        return map;
    }

    public Map<Integer, List<String>> getSystemSettingData() {
        Map<Integer, List<String>> map = new LinkedHashMap<>();
        map.put(0, Arrays.asList(R.mipmap.network + "", "開機源"));
        map.put(1, Arrays.asList(R.mipmap.network + "", "數位相框"));
        map.put(2, Arrays.asList(R.mipmap.network + "", "系統訊息"));
        map.put(3, Arrays.asList(R.mipmap.network + "", "系統升級"));
        map.put(4, Arrays.asList(R.mipmap.network + "", "恢復出廠"));
        return map;
    }

}
