package com.example.solidpro.RecyclerModel;

import android.graphics.Rect;
import android.view.View;

import androidx.recyclerview.widget.RecyclerView;

public class FavAppItemDecoration extends RecyclerView.ItemDecoration {

    private int spanCount;
    private int spacing;

    public FavAppItemDecoration(int spanCount, int spacing) {
        this.spanCount = spanCount;
        this.spacing = spacing;
    }

    @Override
    public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {
        int position = parent.getChildAdapterPosition(view); // item position
        int column = position % spanCount; // item column


//        outRect.left = (spacing - column * spacing / spanCount)/3 ; // spacing - column * ((1f / spanCount) * spacing)
//        outRect.right = (column + 1) * spacing / spanCount;
        outRect.left = spacing / 6;
        outRect.right = spacing / 6;

        if (position >= spanCount) {
//            outRect.top = (int) ((((global.windowHeigh * 0.78 * 0.8) - (global.windowWidth * 0.52 / 3 * 0.85) * 2)-spacing/6) / 2 );
            // 下排
            outRect.top = spacing / 6 - spacing / 12;
            outRect.bottom = spacing / 12;
        } else {
            // 上排
            outRect.top = spacing / 12 ;
//            outRect.bottom = (int) ((((global.windowHeigh * 0.78 * 0.8) - (global.windowWidth * 0.52 / 3 * 0.85) * 2)-spacing/6) / 2); // item bottom
            outRect.bottom = spacing / 6 - spacing / 12;
        }
    }

}