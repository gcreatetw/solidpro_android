package com.example.solidpro.View;

import android.app.Dialog;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;

import androidx.fragment.app.DialogFragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.solidpro.R;
import com.example.solidpro.RecyclerModel.Adapter_InstalledApps;
import com.example.solidpro.RecyclerModel.GridSpacingItemDecoration;
import com.example.solidpro.RecyclerModel.ObjectInstalledAppsList;
import com.example.solidpro.global;

import java.util.ArrayList;
import java.util.List;


public class DialogFragment_installedApp extends DialogFragment {
    private RecyclerView recyclerView;
    private static ArrayList<ObjectInstalledAppsList> res;
    private F_main.UpdateFavListReceiver mupdateFavListReceiver = new F_main.UpdateFavListReceiver();
    private Intent intent;


    public DialogFragment_installedApp() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(DialogFragment.STYLE_NORMAL,R.style.Fullscreen);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_dialog_installed_app, container, false);
        view.requestFocus();
        recyclerView = view.findViewById(R.id.installAppsRecycler2);

        setupRecyclerView();
        return view;
    }

    private void setupRecyclerView() {

        Adapter_InstalledApps adapter = new Adapter_InstalledApps(getInstalledApps(false));
        recyclerView.setLayoutManager(new GridLayoutManager(getActivity(), 5));
        recyclerView.setAdapter(adapter);
//        recyclerView.addItemDecoration(new GridSpacingItemDecoration(5, (dialogWidth - 264 * 5) / 6, true));
        recyclerView.addItemDecoration(new GridSpacingItemDecoration(5, 0, false));
        adapter.setOnItemClickListener(new Adapter_InstalledApps.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                if (global.isComeFormFav) {
                    IntentFilter intentFilter = new IntentFilter("update");
                    getActivity().getApplicationContext().registerReceiver(mupdateFavListReceiver, intentFilter);
                    intent = new Intent();
                    intent.setAction("update");
                    getActivity().getApplicationContext().sendBroadcast(intent);

                    global.isComeFormFav = false;
                    getDialog().cancel();
                } else {
                    PackageManager packageManager = getActivity().getPackageManager();
                    startActivity(packageManager.getLaunchIntentForPackage(res.get(position).getPackageName()));
                    getDialog().cancel();
                }
            }
        });
    }


    private ArrayList<ObjectInstalledAppsList> getInstalledApps(boolean getSysPackages) {
        res = new ArrayList<>();
        List<PackageInfo> packs = getActivity().getPackageManager().getInstalledPackages(0);

        for (int i = 0; i < packs.size(); i++) {
            PackageInfo p = packs.get(i);
            ApplicationInfo appInfo = p.applicationInfo;
            // 過濾無 Activity 的 APK
            Intent intent = getActivity().getPackageManager().getLaunchIntentForPackage(appInfo.packageName);
            if (intent != null) {
                ObjectInstalledAppsList newInfo = new ObjectInstalledAppsList();
                newInfo.setAppImg(p.applicationInfo.loadIcon(getActivity().getPackageManager()));
                newInfo.setAppName(p.applicationInfo.loadLabel(getActivity().getPackageManager()).toString());
                newInfo.setPackageName(p.packageName);
                res.add(newInfo);
            }

        }
        return res;
    }



    @Override
    public void onDestroy() {
        super.onDestroy();
        if (intent != null) {
            getActivity().getApplicationContext().unregisterReceiver(mupdateFavListReceiver);
        }
    }
}