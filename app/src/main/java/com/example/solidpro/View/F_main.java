package com.example.solidpro.View;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.solidpro.R;
import com.example.solidpro.RecyclerModel.Adapter_appIcon;
import com.example.solidpro.RecyclerModel.FavAppItemDecoration;
import com.example.solidpro.RecyclerModel.ObjectFavApp;
import com.example.solidpro.global;

import java.io.ByteArrayOutputStream;


public class F_main extends Fragment {

    public static final String TAG = "F_main";
    private View view;

    //---------- Installed Apps ----------
    private ImageView img_appCenter;
    private static RecyclerView rv_appIcons;
    private static Adapter_appIcon adapter;
    private ImageButton tools;
    private ImageView img_appMarket;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        view = inflater.inflate(R.layout.fragment_main, container, false);
        findView(view);
        veiwFocus();
        viewAction();

        //-------------------------------------   Favorite App Link      -------------------------------------
        rv_appIcons = view.findViewById(R.id.rv_appicon);
        rv_appIcons.setHasFixedSize(true);
        rv_appIcons.setLayoutManager(new GridLayoutManager(getActivity(), 3));
        rv_appIcons.addItemDecoration(new FavAppItemDecoration(3, (int) (((global.windowWidth * 0.52 * 0.15)))));

        adapter = new Adapter_appIcon(global.favAppsList);
        rv_appIcons.setAdapter(adapter);
        adapter.setOnItemClickListener(new Adapter_appIcon.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                if (global.favAppsList.get(position).getFavAppPkgAddress().equals("")) {
                    global.isComeFormFav = true;
                    global.favSelectedPosition = position;
                    DialogFragment installedAppDF = new DialogFragment_installedApp();
                    installedAppDF.show(getActivity().getSupportFragmentManager(), "simple dialog");

                } else {
                    PackageManager packageManager = getActivity().getPackageManager();
                    startActivity(packageManager.getLaunchIntentForPackage(global.favAppsList.get(position).getFavAppPkgAddress()));
                }
            }
        });

        adapter.setOnLongClickListener(new Adapter_appIcon.OnLongClickListener() {
            @Override
            public void onLongClick(View view, final int position) {
                if (!global.favAppsList.get(position).getFavAppName().equals("")) {
                    AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getActivity());
                    final View customLayout = getLayoutInflater().inflate(R.layout.fragment_dialog_keyin_wifi_pwd, null);
                    alertDialogBuilder.setView(customLayout);
                    final AlertDialog alertDialog = alertDialogBuilder.create();
                    //----      Setup View      ----
                    final EditText editPwd = customLayout.findViewById(R.id.edit_pwd);
                    final Button btn_ok = customLayout.findViewById(R.id.btn_ok);
                    final Button btn_cancel = customLayout.findViewById(R.id.btn_cancel);
                    final TextView alertDialogTitle = customLayout.findViewById(R.id.tv_alert_title);


                    alertDialogTitle.setText("是否將" + global.favAppsList.get(position).getFavAppName() + "移除最愛");
                    alertDialogTitle.requestFocus();

                    editPwd.setVisibility(View.GONE);

                    //----      Setup ViewAction      ----
                    btn_ok.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            global.favSelectedPosition = position;
                            deleteFav();
                            alertDialog.dismiss();
                        }
                    });

                    btn_cancel.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            alertDialog.dismiss();
                        }
                    });


                    //----      Setup ViewFocus     ----
                    alertDialogTitle.setOnFocusChangeListener(new View.OnFocusChangeListener() {
                        @Override
                        public void onFocusChange(View v, boolean hasFocus) {
                            if (!hasFocus)
                                alertDialogTitle.setFocusable(false);
                        }
                    });
                    btn_ok.setOnFocusChangeListener(new View.OnFocusChangeListener() {
                        @Override
                        public void onFocusChange(View v, boolean hasFocus) {
                            if (hasFocus)
                                btn_ok.setBackground(ContextCompat.getDrawable(global.context, R.drawable.bg_focus_alertdialog));
                            else
                                btn_ok.setBackground(null);
                        }
                    });
                    btn_cancel.setOnFocusChangeListener(new View.OnFocusChangeListener() {
                        @Override
                        public void onFocusChange(View v, boolean hasFocus) {
                            if (hasFocus)
                                btn_cancel.setBackground(ContextCompat.getDrawable(global.context, R.drawable.bg_focus_alertdialog));
                            else
                                btn_cancel.setBackground(null);
                        }
                    });

                    alertDialog.show();
                    alertDialog.getWindow().setLayout((int) (global.windowWidth * 0.3), (int) (global.windowHeigh * 0.3));
                }
            }

        });


        return view;

    }

    private void findView(View view) {
        img_appMarket = view.findViewById(R.id.img_market);
        img_appMarket.getLayoutParams().height = (int) (((global.windowWidth * 0.52) / 3) * 0.86);
        img_appMarket.getLayoutParams().width = (int) (global.windowWidth * 0.384);

        img_appCenter = view.findViewById(R.id.img_appCenter);
        img_appCenter.getLayoutParams().height = (int) (((global.windowWidth * 0.52) / 3) * 0.86);
        img_appCenter.getLayoutParams().width = (int) (global.windowWidth * 0.384);

        tools = view.findViewById(R.id.imgBtn_tools);
    }

    private void veiwFocus() {
        img_appMarket.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                int item_h = (int) (((global.windowWidth * 0.52) / 3) * 0.86);
                int item_w = (int) (global.windowWidth * 0.384);
                if (hasFocus) {
                    img_appMarket.setCropToPadding(true);
                    img_appMarket.setBackground(ContextCompat.getDrawable(getActivity(), R.drawable.bg_focus_main));
                    img_appMarket.getLayoutParams().height = (int) ((item_h) * 1.13);
                    img_appMarket.getLayoutParams().width = (int) (item_w * 1.13);
                } else {
                    img_appMarket.setCropToPadding(false);
                    img_appMarket.setBackground(null);
                    img_appMarket.getLayoutParams().height = (int) (((global.windowWidth * 0.52) / 3) * 0.9);
                    img_appMarket.getLayoutParams().width = (int) (item_w);
                }


            }
        });

        img_appCenter.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                int item_h = (int) (((global.windowWidth * 0.52) / 3) * 0.86);
                int item_w = (int) (global.windowWidth * 0.384);
                if (hasFocus) {
                    img_appCenter.setCropToPadding(true);
                    img_appCenter.setBackground(ContextCompat.getDrawable(getActivity(), R.drawable.bg_focus_main));
                    img_appCenter.getLayoutParams().height = (int) ((item_h) * 1.13);
                    img_appCenter.getLayoutParams().width = (int) (item_w * 1.13);
                } else {
                    img_appCenter.setCropToPadding(false);
                    img_appCenter.setBackground(null);
                    img_appCenter.getLayoutParams().height = (int) (((global.windowWidth * 0.52) / 3) * 0.9);
                    img_appCenter.getLayoutParams().width = (int) (item_w);
                }

            }
        });

        tools.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus) {
                    tools.setCropToPadding(true);
                    tools.setBackground(ContextCompat.getDrawable(getActivity(), R.drawable.bg_focus_main));
                    DialogFragment setupDF = new DialogFragment_MainSetup();
                    setupDF.show(getActivity().getSupportFragmentManager(), "setup dialog");

                } else {
                    tools.setCropToPadding(false);
                    tools.setBackground(null);
                }

            }
        });
    }

    private void viewAction() {
        //-------------------------------------    APP 中心       -------------------------------------
        img_appCenter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DialogFragment installedAppDF = new DialogFragment_installedApp();
                installedAppDF.show(getActivity().getSupportFragmentManager(), "simple dialog");
            }
        });

        //-------------------------------------   tools      -------------------------------------
        tools.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DialogFragment setupDF = new DialogFragment_MainSetup();
                setupDF.show(getActivity().getSupportFragmentManager(), "setup dialog");
            }
        });
    }

    //------------------------------------------UpdateFavList BroadcastReceiver -----------------------------------------
    public static class UpdateFavListReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            if ("update".equals(intent.getAction())) {
                updateFav();
            }
        }
    }


    private static void updateFav() {
        global.favAppsList.remove(global.favSelectedPosition);
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        getBitmapFromDrawable(global.favSelectedImg).compress(Bitmap.CompressFormat.PNG, 100, stream);

        byte[] bitmapdata = stream.toByteArray();
        global.favAppsList.add(global.favSelectedPosition, new ObjectFavApp(bitmapdata, global.favSelectedAppPkgAddr, global.favSelectedAppName));

        rv_appIcons.setAdapter(adapter);
        global.saveFavAppList();
    }

    private static void deleteFav() {
        global.favAppsList.remove(global.favSelectedPosition);
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        getBitmapFromDrawable(ContextCompat.getDrawable(global.context, R.drawable.icon_add)).compress(Bitmap.CompressFormat.PNG, 100, stream);

        byte[] bitmapdata = stream.toByteArray();
        global.favAppsList.add(global.favSelectedPosition, new ObjectFavApp(bitmapdata, "", ""));

        rv_appIcons.setAdapter(adapter);
        global.saveFavAppList();
    }

    @NonNull
    public static Bitmap getBitmapFromDrawable(@NonNull Drawable drawable) {
        final Bitmap bmp = Bitmap.createBitmap(drawable.getIntrinsicWidth(), drawable.getIntrinsicHeight(), Bitmap.Config.ARGB_8888);
        final Canvas canvas = new Canvas(bmp);
        canvas.drawColor(Color.TRANSPARENT, PorterDuff.Mode.CLEAR);
        drawable.setBounds(0, 0, canvas.getWidth(), canvas.getHeight());
        drawable.draw(canvas);
        return bmp;
    }


}