package com.example.solidpro.View;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;

import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;

import com.example.solidpro.R;
import com.example.solidpro.global;


public class F_lunchSelectLanguage extends Fragment {


    private View view;
    private Button btn_TCselected;
    private Button btn_ENselected;
    private LinearLayout ly_lunchNext;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        view = inflater.inflate(R.layout.fragment_lunch_select_language, container, false);
        findView(view);
        viewFocus();
        return view;
    }

    private void findView(View view) {
        btn_TCselected = view.findViewById(R.id.btn_chineseSelected);
        btn_ENselected = view.findViewById(R.id.btn_englishSelected);
        ly_lunchNext = view.findViewById(R.id.ly_lunchSelectLanguageNext);
    }

    private void viewFocus() {

        btn_TCselected.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus) {
                    btn_TCselected.setBackground(ContextCompat.getDrawable(getActivity(), R.drawable.bg_focus_solid_black_radius10));

                } else {
                    btn_TCselected.setBackground(ContextCompat.getDrawable(getActivity(), R.drawable.bg_solid_black_radius10));
                }
            }
        });

        btn_ENselected.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus) {
                    btn_ENselected.setBackground(ContextCompat.getDrawable(getActivity(), R.drawable.bg_focus_solid_black_radius10));
                } else {
                    btn_ENselected.setBackground(ContextCompat.getDrawable(getActivity(), R.drawable.bg_solid_black_radius10));
                }
            }
        });

        ly_lunchNext.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus) {
                    ly_lunchNext.setBackground(ContextCompat.getDrawable(getActivity(), R.drawable.bg_focus_solid_black_radius10));

                } else {
                    ly_lunchNext.setBackground(null);
                }
            }
        });

        ly_lunchNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new global().lunchReplaceFragment(getActivity(), new F_lunchSelectWifi());
            }
        });
    }
}