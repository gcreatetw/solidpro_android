package com.example.solidpro.View;

import android.content.Intent;
import android.os.Bundle;
import android.view.KeyEvent;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentManager;

import com.example.solidpro.MainActivity;
import com.example.solidpro.R;
import com.example.solidpro.global;


public class Activity_LunchSetting extends AppCompatActivity {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lunch_setting);
        global.getWindowSize(this);

        //-------------------------------------    Init FavAppList      --------------------------------
        global.context = this;
        if (global.getIsFirstTimeOpenApp()) {
            global.initFavAppsList();
            global.initSystemKeystoneCorrectionSetting();
            global.setIsFirstTimeOpenApp(true);
            new global().lunchReplaceFragment(this, new F_lunchStart());
        } else {
            global.getFavAppsList();
            Intent intent = new Intent(this, MainActivity.class);
            startActivity(intent);
            this.finish();
        }


    }

    @Override
    public void onBackPressed() {
        FragmentManager fragmentManager = getSupportFragmentManager();
        if (fragmentManager.getBackStackEntryCount() > 1) {
            fragmentManager.popBackStackImmediate();

        } else {
            finish();
        }
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {

        switch (keyCode) {

            case KeyEvent.KEYCODE_ENTER:     //确定键enter
            case KeyEvent.KEYCODE_DPAD_CENTER:

                break;

            case KeyEvent.KEYCODE_BACK:    //返回键

//                break;
                return true;   //这里由于break会退出，所以我们自己要处理掉 不返回上一层

            case KeyEvent.KEYCODE_SETTINGS: //设置键


                break;

            case KeyEvent.KEYCODE_DPAD_DOWN:   //向下键
                /*    实际开发中有时候会触发两次，所以要判断一下按下时触发 ，松开按键时不触发
                 *    exp:KeyEvent.ACTION_UP
                 */
                if (event.getAction() == KeyEvent.ACTION_DOWN) {
                }

                break;

            case KeyEvent.KEYCODE_DPAD_UP:   //向上键
                break;

            case KeyEvent.KEYCODE_0:   //数字键0
                break;

            case KeyEvent.KEYCODE_DPAD_LEFT: //向左键

                break;

            case KeyEvent.KEYCODE_DPAD_RIGHT:  //向右键
                break;

            case KeyEvent.KEYCODE_INFO:    //info键


                break;

            case KeyEvent.KEYCODE_PAGE_DOWN:     //向上翻页键
            case KeyEvent.KEYCODE_MEDIA_NEXT:


                break;


            case KeyEvent.KEYCODE_PAGE_UP:     //向下翻页键
            case KeyEvent.KEYCODE_MEDIA_PREVIOUS:


                break;

            case KeyEvent.KEYCODE_VOLUME_UP:   //调大声音键


                break;

            case KeyEvent.KEYCODE_VOLUME_DOWN: //降低声音键


                break;
            case KeyEvent.KEYCODE_VOLUME_MUTE: //禁用声音

                break;

            default:
                break;
        }

        return super.onKeyDown(keyCode, event);

    }

}