package com.example.solidpro.View;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.core.content.ContextCompat;
import androidx.fragment.app.DialogFragment;

import com.example.solidpro.R;

import static com.example.solidpro.global.context;


public class DialogFragment_MainSetup extends DialogFragment {

    private LinearLayout tools_hdmi, tools_usb1, tools_usb2, tools_sd, tools_sound;
    private ImageView img_hdmi, img_usb1, img_usb2, img_sd, img_sound;
    private TextView tv_hdmi, tv_usb1, tv_usb2, tv_sd, tv_sound;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_dialog_main_setup, container, false);
        findView(view);
        viewFocus();
        viewAction();
        return view;
    }


    @Override
    public void onStart() {
        super.onStart();

        Dialog dialog = getDialog();
        if (dialog != null) {

            WindowManager.LayoutParams lp = dialog.getWindow().getAttributes();
            lp.dimAmount = 0f;

            dialog.getWindow().setGravity(Gravity.BOTTOM);        //设置弹出位置
            dialog.getWindow().setAttributes(lp);               //取消dialog周邊變暗的效果
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        }
    }


    private void findView(View view) {
        //-- HDMI --
        tools_hdmi = view.findViewById(R.id.ly_tools_hdmi);
        img_hdmi = view.findViewById(R.id.img_tool_hdmi);
        tv_hdmi = view.findViewById(R.id.tv_tool_hdmi);
        //-- USB1 --
        tools_usb1 = view.findViewById(R.id.ly_tools_USB1);
        img_usb1 = view.findViewById(R.id.img_tool_usb1);
        tv_usb1 = view.findViewById(R.id.tv_tool_usb1);
        //-- USB2 --
        tools_usb2 = view.findViewById(R.id.ly_tools_USB2);
        img_usb2 = view.findViewById(R.id.img_tool_usb2);
        tv_usb2 = view.findViewById(R.id.tv_tool_usb2);
        //-- SD Card --
        tools_sd = view.findViewById(R.id.ly_tools_SD);
        img_sd = view.findViewById(R.id.img_tool_sd);
        tv_sd = view.findViewById(R.id.tv_tool_sd);
        //-- Sound --
        tools_sound = view.findViewById(R.id.ly_tools_Sound);
        img_sound = view.findViewById(R.id.img_tool_sound);
        tv_sound = view.findViewById(R.id.tv_tool_sound);

    }

    private void viewFocus() {
        tools_hdmi.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @SuppressLint("ResourceType")
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                settingLayout(img_hdmi,tv_hdmi,hasFocus);
            }
        });

        tools_usb1.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @SuppressLint("ResourceType")
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                settingLayout(img_usb1,tv_usb1,hasFocus);
            }
        });

        tools_usb2.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @SuppressLint("ResourceType")
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                settingLayout(img_usb2,tv_usb2,hasFocus);

            }
        });
        tools_sd.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @SuppressLint("ResourceType")
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                settingLayout(img_sd,tv_sd,hasFocus);

            }
        });
        tools_sound.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @SuppressLint("ResourceType")
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                settingLayout(img_sound,tv_sound,hasFocus);
            }
        });
    }

    private void viewAction() {
        tools_sound.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(getActivity(), Activity_SystemSetting.class);
                startActivity(intent);
                getDialog().cancel();
            }
        });
    }

    private void settingLayout(ImageView imageView,TextView textView,Boolean hasFocus){
        if (hasFocus) {
            imageView.setColorFilter(ContextCompat.getColor(context, R.color.blue0080CB), android.graphics.PorterDuff.Mode.MULTIPLY);
            textView.setTextColor(ContextCompat.getColor(context, R.color.blue0080CB));
        } else {
            imageView.setColorFilter(ContextCompat.getColor(context, R.color.whiteffffff), android.graphics.PorterDuff.Mode.MULTIPLY);
            textView.setTextColor(ContextCompat.getColor(context, R.color.whiteffffff));
        }
    }

}