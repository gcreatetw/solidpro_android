package com.example.solidpro.View;

import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.SeekBar;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.content.ContextCompat;

import com.example.solidpro.R;
import com.example.solidpro.global;

import static com.example.solidpro.global.context;

public class Activity_FocusMode extends AppCompatActivity {

    private ConstraintLayout focusMode1,focusMode2,focusMode3,focusMode4;
    private TextView tv_focus_mode1,tv_focus_mode2,tv_focus_mode3,tv_focus_mode4,tv_focus_mode4_1;
    private SeekBar seekBar1,seekBar2,seekBar3;
    private TextView seekBarTxt1,seekBarTxt2,seekBarTxt3;
    private ImageView img_focus_mode1;
    private TextView tv_focus_reset;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_focus_mode);

        findView();
        viewAction();
        viewFocus();
    }


    private void findView() {
        focusMode1 = findViewById(R.id.cl_focus_mode1);
        focusMode2 = findViewById(R.id.cl_focus_mode2);
        focusMode3 = findViewById(R.id.cl_focus_mode3);
        focusMode4 = findViewById(R.id.cl_focus_mode4);

        tv_focus_mode1 = findViewById(R.id.tv_focus_mode1);
        tv_focus_mode2 = findViewById(R.id.tv_focus_mode2);
        tv_focus_mode3 = findViewById(R.id.tv_focus_mode3);
        tv_focus_mode4 = findViewById(R.id.tv_focus_mode4);
        tv_focus_mode4_1 = findViewById(R.id.tv_focus_mode4_1);
        tv_focus_reset = findViewById(R.id.tv_focus_reset);

        seekBar1 = findViewById(R.id.focus_seekBar1);
        seekBar1.setProgress(global.getSystemSettingFocusModeGlobalZoom());

        seekBar2 = findViewById(R.id.focus_seekBar2);
        seekBar2.setProgress(global.getSystemSettingFocusModeVerticalZoom());

        seekBar3 = findViewById(R.id.focus_seekBar3);
        seekBar3.setProgress(global.getSystemSettingFocusModeHorizonZoom());

        seekBarTxt1 = findViewById(R.id.tv_seekBarTxt1);
        seekBarTxt1.setText(global.getSystemSettingFocusModeGlobalZoom() + "");

        seekBarTxt2 = findViewById(R.id.tv_seekBarTxt2);
        seekBarTxt2.setText(global.getSystemSettingFocusModeVerticalZoom() + "");

        seekBarTxt3 = findViewById(R.id.tv_seekBarTxt3);
        seekBarTxt3.setText(global.getSystemSettingFocusModeHorizonZoom() + "");

        img_focus_mode1 = findViewById(R.id.img_focus_mode1);


    }

    private void viewAction() {
        seekBar1.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                seekBarTxt1.setText(String.valueOf(progress));
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });

        seekBar2.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                seekBarTxt2.setText(String.valueOf(progress));
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });

        seekBar3.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                seekBarTxt3.setText(String.valueOf(progress));
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });

        tv_focus_reset.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                seekBarTxt1.setText("0");
                seekBarTxt2.setText("0");
                seekBarTxt3.setText("0");
                seekBar1.setProgress(0);
                seekBar2.setProgress(0);
                seekBar3.setProgress(0);

            }
        });

    }

    private void viewFocus() {

        seekBar1.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                settingLayout(focusMode1, tv_focus_mode1, seekBarTxt1, seekBar1, hasFocus);
            }
        });

        seekBar2.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                settingLayout(focusMode2, tv_focus_mode2, seekBarTxt2, seekBar2, hasFocus);
            }
        });

        seekBar3.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                settingLayout(focusMode3, tv_focus_mode3, seekBarTxt3, seekBar3, hasFocus);
            }
        });

        focusMode4.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                settingLayout(focusMode4, tv_focus_mode4, tv_focus_mode4_1, img_focus_mode1, hasFocus);
            }
        });

        tv_focus_reset.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus) {
                    tv_focus_reset.setBackground(ContextCompat.getDrawable(Activity_FocusMode.this, R.drawable.bg_focus_tools_radius30));
                    tv_focus_reset.setScaleX((float) 1.15);
                    tv_focus_reset.setScaleY((float) 1.15);
                    tv_focus_reset.setTextColor(ContextCompat.getColor(context, R.color.blue0080CB));

                } else {
                    tv_focus_reset.setBackground(ContextCompat.getDrawable(Activity_FocusMode.this, R.drawable.bg_solid_black_radius30));
                    tv_focus_reset.setScaleX((float) 1);
                    tv_focus_reset.setScaleY((float) 1);
                    tv_focus_reset.setTextColor(ContextCompat.getColor(context, R.color.whiteffffff));
                }
            }
        });

    }


    private void settingLayout(final ConstraintLayout constraintLayout, final TextView title, final TextView seekBarTxt, SeekBar seekBar, Boolean hasFocus) {
        if (hasFocus) {
            constraintLayout.setBackground(ContextCompat.getDrawable(Activity_FocusMode.this, R.drawable.bg_focus_tools));
            constraintLayout.setScaleX((float) 1.1);
            constraintLayout.setScaleY((float) 1.1);
            title.setTextColor(ContextCompat.getColor(context, R.color.blue0080CB));
            seekBarTxt.setTextColor(ContextCompat.getColor(context, R.color.blue0080CB));

        } else {
            constraintLayout.setBackground(ContextCompat.getDrawable(Activity_FocusMode.this, R.drawable.bg_solid_black_radius10));
            constraintLayout.setScaleX((float) 1);
            constraintLayout.setScaleY((float) 1);
            title.setTextColor(ContextCompat.getColor(context, R.color.whiteffffff));
            seekBarTxt.setTextColor(ContextCompat.getColor(context, R.color.whiteffffff));
        }
    }

    private void settingLayout(ConstraintLayout constraintLayout, TextView title, TextView subtitle, ImageView icon, Boolean hasFocus) {
        if (hasFocus) {
            constraintLayout.setBackground(ContextCompat.getDrawable(Activity_FocusMode.this, R.drawable.bg_focus_tools));
            constraintLayout.setScaleX((float) 1.1);
            constraintLayout.setScaleY((float) 1.1);
            title.setTextColor(ContextCompat.getColor(context, R.color.blue0080CB));
            subtitle.setTextColor(ContextCompat.getColor(context, R.color.blue0080CB));
            icon.setColorFilter(ContextCompat.getColor(context, R.color.blue0080CB));

        } else {
            constraintLayout.setBackground(ContextCompat.getDrawable(Activity_FocusMode.this, R.drawable.bg_solid_black_radius10));
            constraintLayout.setScaleX((float) 1);
            constraintLayout.setScaleY((float) 1);
            title.setTextColor(ContextCompat.getColor(context, R.color.whiteffffff));
            subtitle.setTextColor(ContextCompat.getColor(context, R.color.whiteffffff));
            icon.setColorFilter(ContextCompat.getColor(context, R.color.whiteffffff));

        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        global.setSystemSettingFocusModeGlobalZoom(Integer.parseInt(seekBarTxt1.getText().toString()));
        global.setSystemSettingFocusModeVerticalZoom(Integer.parseInt(seekBarTxt2.getText().toString()));
        global.setSystemSettingFocusModeHorizonZoom(Integer.parseInt(seekBarTxt3.getText().toString()));
    }
}