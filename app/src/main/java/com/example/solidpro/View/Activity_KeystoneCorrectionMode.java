package com.example.solidpro.View;

import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.content.ContextCompat;

import com.example.solidpro.R;
import com.example.solidpro.global;

import static com.example.solidpro.global.context;
import static com.example.solidpro.global.getKeystoneCorrectionSetting;

public class Activity_KeystoneCorrectionMode extends AppCompatActivity {

    private ConstraintLayout keystoneCorrection_mode1,keystoneCorrection_mode2,keystoneCorrection_mode3,keystoneCorrection_mode4;
    private TextView tv_keystoneCorrection_mode1,tv_keystoneCorrection_mode2,tv_keystoneCorrection_mode3,tv_keystoneCorrection_mode3_1,tv_keystoneCorrection_mode4,tv_keystoneCorrection_mode4_1;
    private ImageView img_keystoneCorrection_mode1,img_keystoneCorrection_mode2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_keystonecorrection_mode);

        getKeystoneCorrectionSetting();
        findView();
        viewFocus();
        viewAction();
    }


    private void findView() {
        keystoneCorrection_mode1 = findViewById(R.id.cl_KeystoneCorrection_mode1);
        keystoneCorrection_mode2 = findViewById(R.id.cl_KeystoneCorrection_mode2);
        keystoneCorrection_mode3 = findViewById(R.id.cl_KeystoneCorrection_mode3);
        keystoneCorrection_mode4 = findViewById(R.id.cl_KeystoneCorrection_mode4);

        tv_keystoneCorrection_mode1 = findViewById(R.id.tv_KeystoneCorrection_mode1);
        tv_keystoneCorrection_mode2 = findViewById(R.id.tv_KeystoneCorrection_mode2);
        tv_keystoneCorrection_mode3 = findViewById(R.id.tv_KeystoneCorrection_mode3);
        tv_keystoneCorrection_mode3_1 = findViewById(R.id.tv_KeystoneCorrection_mode3_1);
        if (global.keystoneCorrectionDefaultSetting.get(0)) {
            tv_keystoneCorrection_mode3_1.setText("打開");
        } else {
            tv_keystoneCorrection_mode3_1.setText("關閉");
        }

        tv_keystoneCorrection_mode4 = findViewById(R.id.tv_KeystoneCorrection_mode4);
        tv_keystoneCorrection_mode4_1 = findViewById(R.id.tv_KeystoneCorrection_mode4_1);
        if (global.keystoneCorrectionDefaultSetting.get(1)) {
            tv_keystoneCorrection_mode4_1.setText("打開");
        } else {
            tv_keystoneCorrection_mode4_1.setText("關閉");
        }

        img_keystoneCorrection_mode1 = findViewById(R.id.img_KeystoneCorrection_mode1);
        img_keystoneCorrection_mode2 = findViewById(R.id.img_KeystoneCorrection_mode2);

    }

    private void viewFocus() {

        keystoneCorrection_mode1.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                settingLayout(keystoneCorrection_mode1, tv_keystoneCorrection_mode1, null, img_keystoneCorrection_mode1, hasFocus);
            }
        });

        keystoneCorrection_mode2.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                settingLayout(keystoneCorrection_mode2, tv_keystoneCorrection_mode2, null, img_keystoneCorrection_mode2, hasFocus);
            }
        });

        keystoneCorrection_mode3.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                settingLayout(keystoneCorrection_mode3, tv_keystoneCorrection_mode3, tv_keystoneCorrection_mode3_1, null, hasFocus);
            }
        });

        keystoneCorrection_mode4.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                settingLayout(keystoneCorrection_mode4, tv_keystoneCorrection_mode4, tv_keystoneCorrection_mode4_1, null, hasFocus);
            }
        });
    }

    private void viewAction() {
        keystoneCorrection_mode3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (global.keystoneCorrectionDefaultSetting.get(0)) {
                    global.keystoneCorrectionDefaultSetting.remove(0);
                    global.keystoneCorrectionDefaultSetting.add(0, false);
                    global.saveKeystoneCorrectionSetting();
                    tv_keystoneCorrection_mode3_1.setText("關閉");
                } else {
                    global.keystoneCorrectionDefaultSetting.remove(0);
                    global.keystoneCorrectionDefaultSetting.add(0, true);
                    global.saveKeystoneCorrectionSetting();
                    tv_keystoneCorrection_mode3_1.setText("打開");
                }
            }
        });

        keystoneCorrection_mode4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (global.keystoneCorrectionDefaultSetting.get(1)) {
                    global.keystoneCorrectionDefaultSetting.remove(1);
                    global.keystoneCorrectionDefaultSetting.add(1, false);
                    global.saveKeystoneCorrectionSetting();
                    tv_keystoneCorrection_mode4_1.setText("關閉");
                } else {
                    global.keystoneCorrectionDefaultSetting.remove(1);
                    global.keystoneCorrectionDefaultSetting.add(1, true);
                    global.saveKeystoneCorrectionSetting();
                    tv_keystoneCorrection_mode4_1.setText("打開");
                }
            }
        });

    }

    private void settingLayout(ConstraintLayout constraintLayout, TextView title, TextView subtitle, ImageView icon, Boolean hasFocus) {
        if (hasFocus) {
            constraintLayout.setBackground(ContextCompat.getDrawable(Activity_KeystoneCorrectionMode.this, R.drawable.bg_focus_tools));
            constraintLayout.setScaleX((float) 1.1);
            constraintLayout.setScaleY((float) 1.1);
            title.setTextColor(ContextCompat.getColor(context, R.color.blue0080CB));
            if (subtitle != null) {
                subtitle.setTextColor(ContextCompat.getColor(context, R.color.blue0080CB));
            }
            if (icon != null) {
                icon.setColorFilter(ContextCompat.getColor(context, R.color.blue0080CB));
            }
        } else {
            constraintLayout.setBackground(ContextCompat.getDrawable(Activity_KeystoneCorrectionMode.this, R.drawable.bg_solid_black_radius10));
            constraintLayout.setScaleX((float) 1);
            constraintLayout.setScaleY((float) 1);
            title.setTextColor(ContextCompat.getColor(context, R.color.whiteffffff));
            if (subtitle != null) {
                subtitle.setTextColor(ContextCompat.getColor(context, R.color.whiteffffff));
            }
            if (icon != null) {
                icon.setColorFilter(ContextCompat.getColor(context, R.color.whiteffffff));
            }

        }
    }


}