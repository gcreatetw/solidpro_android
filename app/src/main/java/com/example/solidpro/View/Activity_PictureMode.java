package com.example.solidpro.View;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.content.ContextCompat;

import com.example.solidpro.R;
import com.example.solidpro.global;

import static com.example.solidpro.global.context;

public class Activity_PictureMode extends AppCompatActivity {

    private ConstraintLayout picture_mode1,picture_mode2,picture_mode3;
    private TextView tv_picture_mode1,tv_picture_mode2,tv_picture_mode3;
    private ImageView img_picture_mode1,img_picture_mode2,img_picture_mode3;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_picture_mode);

        findView();
        viewFocus();
        viewAction();
        setPictureImgModeVisibility(global.getSystemSettingPictureModeSelectedItem());
    }


    private void findView() {
        picture_mode1 = findViewById(R.id.cl_picture_mode1);
        picture_mode2 = findViewById(R.id.cl_picture_mode2);
        picture_mode3 = findViewById(R.id.cl_picture_mode3);

        tv_picture_mode1 = findViewById(R.id.tv_picture_mode1);
        tv_picture_mode2 = findViewById(R.id.tv_picture_mode2);
        tv_picture_mode3 = findViewById(R.id.tv_picture_mode3);

        img_picture_mode1 = findViewById(R.id.img_picture_mode1);
        img_picture_mode2 = findViewById(R.id.img_picture_mode2);
        img_picture_mode3 = findViewById(R.id.img_picture_mode3);
    }

    private void viewAction() {
        picture_mode1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                global.setSystemSettingPictureModeSelectedItem(0);
                setPictureImgModeVisibility(global.getSystemSettingPictureModeSelectedItem());
            }
        });

        picture_mode2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                global.setSystemSettingPictureModeSelectedItem(1);
                setPictureImgModeVisibility(global.getSystemSettingPictureModeSelectedItem());
            }
        });

        picture_mode3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                global.setSystemSettingPictureModeSelectedItem(2);
                setPictureImgModeVisibility(global.getSystemSettingPictureModeSelectedItem());
            }
        });
    }

    private void viewFocus() {

        picture_mode1.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                settingLayout(picture_mode1, tv_picture_mode1, img_picture_mode1, hasFocus);
            }
        });

        picture_mode2.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                settingLayout(picture_mode2, tv_picture_mode2, img_picture_mode2, hasFocus);
            }
        });

        picture_mode3.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                settingLayout(picture_mode3, tv_picture_mode3, img_picture_mode3, hasFocus);
            }
        });
    }

    private void settingLayout(ConstraintLayout constraintLayout, TextView title, ImageView icon, Boolean hasFocus) {
        if (hasFocus) {
            constraintLayout.setBackground(ContextCompat.getDrawable(Activity_PictureMode.this, R.drawable.bg_focus_tools));
            constraintLayout.setScaleX((float) 1.1);
            constraintLayout.setScaleY((float) 1.1);
            title.setTextColor(ContextCompat.getColor(context, R.color.blue0080CB));
            icon.setColorFilter(ContextCompat.getColor(context, R.color.blue0080CB));
        } else {
            constraintLayout.setBackground(ContextCompat.getDrawable(Activity_PictureMode.this, R.drawable.bg_solid_black_radius10));
            constraintLayout.setScaleX((float) 1);
            constraintLayout.setScaleY((float) 1);
            title.setTextColor(ContextCompat.getColor(context, R.color.whiteffffff));
            icon.setColorFilter(ContextCompat.getColor(context, R.color.whiteffffff));
        }
    }

    private void setPictureImgModeVisibility(int default_selected) {
        switch (default_selected){
            case 0:
                img_picture_mode1.setVisibility(View.VISIBLE);
                img_picture_mode2.setVisibility(View.GONE);
                img_picture_mode3.setVisibility(View.GONE);
                break;
            case 1:
                img_picture_mode1.setVisibility(View.GONE);
                img_picture_mode2.setVisibility(View.VISIBLE);
                img_picture_mode3.setVisibility(View.GONE);
                break;
            case 2:
                img_picture_mode1.setVisibility(View.GONE);
                img_picture_mode2.setVisibility(View.GONE);
                img_picture_mode3.setVisibility(View.VISIBLE);
                break;
        }

    }


}