package com.example.solidpro.View;

import android.content.Intent;
import android.os.Bundle;
import android.provider.Settings;
import android.util.Log;
import android.view.View;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.solidpro.R;
import com.example.solidpro.RecyclerModel.Adapter_setting;
import com.example.solidpro.RecyclerModel.SettingData;

public class Activity_SystemSetting extends AppCompatActivity {

    private RecyclerView rv_wifi, rv_projection, rv_sound, rv_general, rv_system;
    private Adapter_setting adapter_setting;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_system_setting);

        findView();
        viewAction();


    }

    private void findView() {
        rv_wifi = findViewById(R.id.rv_Setting_wifi);
        rv_projection = findViewById(R.id.rv_Setting_projection);
        rv_sound = findViewById(R.id.rv_Setting_sound);
        rv_general = findViewById(R.id.rv_Setting_general);
        rv_system = findViewById(R.id.rv_Setting_system);
    }

    private void viewAction() {

        //--------------------------------  Wifi Setting   --------------------------------
        rv_wifi.setHasFixedSize(true);
        rv_wifi.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false));
        adapter_setting = new Adapter_setting(new SettingData().getWifiSettingData());
        rv_wifi.setAdapter(adapter_setting);
        adapter_setting.setOnItemClickListener(new Adapter_setting.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {

                switch (position) {
                    //----    WIFI    -----
                    case 0:
                        startActivity(new Intent(Settings.ACTION_WIFI_SETTINGS));
                        break;
                    //----    藍牙    -----
                    case 1:
                        startActivity(new Intent(Settings.ACTION_BLUETOOTH_SETTINGS));
                        break;
                    //----    無線熱點    -----
                    case 2:
                        Intent tetherSettings = new Intent();
                        tetherSettings.setClassName("com.android.settings", "com.android.settings.TetherSettings");
                        startActivity(tetherSettings);
                        break;
                }
            }
        });

        //--------------------------------  project Setting   --------------------------------
        rv_projection.setHasFixedSize(true);
        rv_projection.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false));
        adapter_setting = new Adapter_setting(new SettingData().getProjectionSettingData());
        rv_projection.setAdapter(adapter_setting);
        adapter_setting.setOnItemClickListener(new Adapter_setting.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {

                switch (position) {
                    //----    畫面模式    -----
                    case 0:
                        startActivity(new Intent(Activity_SystemSetting.this, Activity_PictureMode.class));
                        break;
                    //----    梯形校正    -----
                    case 1:
                        startActivity(new Intent(Activity_SystemSetting.this, Activity_KeystoneCorrectionMode.class));
                        break;
                    //----    投影方式    -----
                    case 2:
                        startActivity(new Intent(Activity_SystemSetting.this, Activity_ProjectionMode.class));
                        break;
                    //----    變焦    -----
                    case 3:
                        startActivity(new Intent(Activity_SystemSetting.this, Activity_FocusMode.class));
                        break;
                    //----    自動對焦    -----
                    case 4:
                        startActivity(new Intent(Activity_SystemSetting.this, Activity_AutoFocus.class));
                        break;
                }
            }
        });

        //--------------------------------  sound Setting   --------------------------------
        rv_sound.setHasFixedSize(true);
        rv_sound.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false));
        adapter_setting = new Adapter_setting(new SettingData().getSoundSettingData());
        rv_sound.setAdapter(adapter_setting);
        adapter_setting.setOnItemClickListener(new Adapter_setting.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                switch (position) {
                    //----    音效    -----
                    case 0:

                        break;
                    //----    按鍵音    -----
                    case 1:

                        break;
                    //----    靜音    -----
                    case 2:

                        break;
                    //----    音響模式    -----
                    case 3:

                        break;

                }
            }
        });

        //--------------------------------  general Setting   --------------------------------
        rv_general.setHasFixedSize(true);
        rv_general.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false));
        rv_general.setAdapter(new Adapter_setting(new SettingData().getGeneralSettingData()));

        //--------------------------------  system Setting   --------------------------------
        rv_system.setHasFixedSize(true);
        rv_system.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false));
        rv_system.setAdapter(new Adapter_setting(new SettingData().getSystemSettingData()));

    }

}