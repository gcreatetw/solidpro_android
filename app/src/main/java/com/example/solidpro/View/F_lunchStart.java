package com.example.solidpro.View;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;

import com.example.solidpro.R;
import com.example.solidpro.global;


public class F_lunchStart extends Fragment {


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_lunch_start, container, false);
        final Button btn_start = view.findViewById(R.id.btn_lunchStart);


        btn_start.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus) {
                    btn_start.setBackground(ContextCompat.getDrawable(getActivity(), R.drawable.bg_focus_solid_black_radius10));

                } else {
                    btn_start.setBackground(ContextCompat.getDrawable(getActivity(), R.drawable.bg_solid_black_radius10));
                }

            }
        });

        btn_start.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new global().lunchReplaceFragment(getActivity(), new F_lunchSelectLanguage());
            }
        });


        return view;
    }
}