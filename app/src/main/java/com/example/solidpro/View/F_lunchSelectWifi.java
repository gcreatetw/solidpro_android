package com.example.solidpro.View;


import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.net.wifi.ScanResult;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.solidpro.MainActivity;
import com.example.solidpro.R;
import com.example.solidpro.RecyclerModel.Adapter_lunchWifiList;
import com.example.solidpro.RecyclerModel.ObjectLunchWifiItem;
import com.example.solidpro.RecyclerModel.customLayoutManager;
import com.example.solidpro.WifiConnector;
import com.example.solidpro.global;

import java.util.ArrayList;
import java.util.List;

import static android.content.Context.WIFI_SERVICE;


public class F_lunchSelectWifi extends Fragment {

    private static final String TAG = "F_lunchSelectWifi";
    private View view;
    private WifiManager wifiManager;
    private List<ObjectLunchWifiItem> wifiItemListst = new ArrayList<>();
    private static final int WIFI_NEED_PASSWORD = 0;
    private static final int WIFI_NO_PASSWORD = 1;
    private static final int WIFI_NOT_CONNECTED = 2;

    public static final String WIFI_AUTH_OPEN = "";
    public static final String WIFI_AUTH_ROAM = "[ESS]";
    private Handler handler = new Handler();
    private RecyclerView rv_lunchWifi;
    private Runnable getWifi;

    private WifiConnector wac;
    private TextView lunchJump;
    private LinearLayout ly_lunchSelectWifiNext;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        view = inflater.inflate(R.layout.fragment_lunch_select_wifi, container, false);
        findView(view);
        viewFocus();
        viewAction();

        handler.post(getWifi);

        return view;
    }

    private void findView(View view) {
        lunchJump = view.findViewById(R.id.tv_lunchJump);
        ly_lunchSelectWifiNext = view.findViewById(R.id.ly_lunchSelectWifiNext);
        rv_lunchWifi = view.findViewById(R.id.rv_lunchWifiList);
        rv_lunchWifi.setLayoutManager(new customLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false));
    }

    private void viewFocus() {
        lunchJump.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus) {
                    lunchJump.setBackground(ContextCompat.getDrawable(getActivity(), R.drawable.bg_focus_solid_black_radius10));
                } else {
                    lunchJump.setBackground(null);
                }
            }
        });


        ly_lunchSelectWifiNext.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus) {
                    ly_lunchSelectWifiNext.setBackground(ContextCompat.getDrawable(getActivity(), R.drawable.bg_focus_solid_black_radius10));

                } else {
                    ly_lunchSelectWifiNext.setBackground(null);

                }
            }
        });
    }

    private void viewAction() {
        getWifi = new Runnable() {
            public void run() {
                handler.postDelayed(this, 10000);
                wifiItemListst.clear();
                getWifiInfo();
                final Adapter_lunchWifiList adapter = new Adapter_lunchWifiList(wifiItemListst);
                adapter.setOnItemClickListener(new Adapter_lunchWifiList.OnItemClickListener() {
                    @Override
                    public void onItemClick(View view, final int position) {
                        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getActivity());
                        final View customLayout = getLayoutInflater().inflate(R.layout.fragment_dialog_keyin_wifi_pwd, null);
                        alertDialogBuilder.setView(customLayout);
                        final AlertDialog alertDialog = alertDialogBuilder.create();
                        //----      Setup View      ----
                        final EditText editPwd = customLayout.findViewById(R.id.edit_pwd);
                        final Button btn_ok = customLayout.findViewById(R.id.btn_ok);
                        final Button btn_cancel = customLayout.findViewById(R.id.btn_cancel);
                        final TextView alertDialogTitle = customLayout.findViewById(R.id.tv_alert_title);

                        alertDialogTitle.setText(wifiItemListst.get(position).getItemName());
                        alertDialogTitle.requestFocus();

                        //----      Setup ViewAction      ----
                        btn_ok.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                try {
                                    wac.connect(wifiItemListst.get(position).getItemName(), editPwd.getText().toString(),
                                            editPwd.getText().toString().equals("") ? WifiConnector.WifiCipherType.WIFICIPHER_NOPASS : WifiConnector.WifiCipherType.WIFICIPHER_WPA);
                                    Toast.makeText(getActivity(), "wifi connected result : " + global.wifiConnectedResult, Toast.LENGTH_LONG).show();
                                    if (global.wifiConnectedResult) {
                                        global.setIsFirstTimeOpenApp(false);
                                        Intent intent = new Intent(getActivity(), MainActivity.class);
                                        startActivity(intent);
                                        getActivity().finish();
                                    }

                                } catch (Exception e) {
                                    Log.d(TAG, String.valueOf(e));
                                }
                            }
                        });

                        btn_cancel.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                alertDialog.dismiss();
                            }
                        });

                        //----      Setup ViewFocus     ----
                        alertDialogTitle.setOnFocusChangeListener(new View.OnFocusChangeListener() {
                            @Override
                            public void onFocusChange(View v, boolean hasFocus) {
                                if (!hasFocus)
                                    alertDialogTitle.setFocusable(false);
                            }
                        });
                        editPwd.setOnFocusChangeListener(new View.OnFocusChangeListener() {
                            @Override
                            public void onFocusChange(View v, boolean hasFocus) {
                                InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(getActivity().INPUT_METHOD_SERVICE);
                                if (hasFocus){
                                    editPwd.setBackground(ContextCompat.getDrawable(global.context, R.drawable.bg_focus_alertdialog));
                                    imm.toggleSoftInput(InputMethodManager.SHOW_IMPLICIT, 0);
                                }else {
                                    editPwd.setBackground(null);
                                    imm.toggleSoftInput(InputMethodManager.HIDE_IMPLICIT_ONLY, 0);
                                }
                            }
                        });
                        btn_ok.setOnFocusChangeListener(new View.OnFocusChangeListener() {
                            @Override
                            public void onFocusChange(View v, boolean hasFocus) {
                                if (hasFocus)
                                    btn_ok.setBackground(ContextCompat.getDrawable(global.context, R.drawable.bg_focus_alertdialog));
                                else
                                    btn_ok.setBackground(null);
                            }
                        });
                        btn_cancel.setOnFocusChangeListener(new View.OnFocusChangeListener() {
                            @Override
                            public void onFocusChange(View v, boolean hasFocus) {
                                if (hasFocus)
                                    btn_cancel.setBackground(ContextCompat.getDrawable(global.context, R.drawable.bg_focus_alertdialog));
                                else
                                    btn_cancel.setBackground(null);
                            }
                        });

                        alertDialog.show();
//                        alertDialog.getWindow().setLayout((int) (global.windowWidth * 0.3), (int) (global.windowHeigh * 0.3));
                    }
                });
                rv_lunchWifi.setAdapter(adapter);
            }
        };

        lunchJump.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                global.setIsFirstTimeOpenApp(true);
                Intent intent = new Intent(getActivity(), MainActivity.class);
                startActivity(intent);
                getActivity().finish();
            }
        });

        ly_lunchSelectWifiNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                global.setIsFirstTimeOpenApp(false);
                Intent intent = new Intent(getActivity(), MainActivity.class);
                startActivity(intent);
                getActivity().finish();
            }
        });
    }


    private void getWifiInfo() {
        ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, 1);
        wifiManager = (WifiManager) getActivity().getSystemService(WIFI_SERVICE);
        wifiManager.startScan();
        Log.d(TAG, wifiManager.startScan() + "");
        showWifiList((ArrayList<ScanResult>) wifiManager.getScanResults());
        wac = new WifiConnector(wifiManager);
    }


    private void showWifiList(ArrayList<ScanResult> list) {
        for (int i = 0; i < list.size(); i++) {
            String strSSID = list.get(i).SSID;
            String strBssid = list.get(i).BSSID;
            String strCapabilities = list.get(i).capabilities;
            boolean ispwd = true;
            int wifiLevel = WifiManager.calculateSignalLevel(list.get(i).level, 5);
            int strLevel = list.get(i).level;
            if (strCapabilities != null && (strCapabilities.equals(WIFI_AUTH_OPEN) || strCapabilities.equals(WIFI_AUTH_ROAM))) {
                ispwd = false;
            }
            Log.d(TAG, "\n" + "SSID: " + strSSID + "\n" + "BSSID: " + strBssid + "\n" + "capabilities： "
                    + strCapabilities + "\n" + "level: " + strLevel + "\n" + "ispwd: " + ispwd);

            wifiItemListst.add(new ObjectLunchWifiItem(strSSID, true, wifiLevel));
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        handler.removeCallbacks(getWifi);
    }
}