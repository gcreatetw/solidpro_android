package com.example.solidpro.View;

import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.content.ContextCompat;

import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.solidpro.R;
import com.example.solidpro.global;

import static com.example.solidpro.global.context;

public class Activity_ProjectionMode extends AppCompatActivity {

    private ConstraintLayout projection_mode1,projection_mode2,projection_mode3,projection_mode4;
    private TextView tv_projection_mode1,tv_projection_mode2,tv_projection_mode3,tv_projection_mode4;
    private ImageView img_projection_mode1,img_projection_mode2,img_projection_mode3,img_projection_mode4;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_projection_mode);

        findView();
        viewFocus();
        viewAction();
        setPictureModeImgVisibility(global.getSystemSettingProjectionModeSelectedItem());

    }

    private void findView() {
        projection_mode1 = findViewById(R.id.cl_projection_mode1);
        projection_mode2 = findViewById(R.id.cl_projection_mode2);
        projection_mode3 = findViewById(R.id.cl_projection_mode3);
        projection_mode4 = findViewById(R.id.cl_projection_mode4);

        tv_projection_mode1 = findViewById(R.id.tv_projection_mode1);
        tv_projection_mode2 = findViewById(R.id.tv_projection_mode2);
        tv_projection_mode3 = findViewById(R.id.tv_projection_mode3);
        tv_projection_mode4 = findViewById(R.id.tv_projection_mode4);

        img_projection_mode1 = findViewById(R.id.img_projection_mode1);
        img_projection_mode2 = findViewById(R.id.img_projection_mode2);
        img_projection_mode3 = findViewById(R.id.img_projection_mode3);
        img_projection_mode4 = findViewById(R.id.img_projection_mode4);
    }

    private void viewAction() {
        projection_mode1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                global.setSystemSettingProjectionModeSelectedItem(0);
                setPictureModeImgVisibility(global.getSystemSettingProjectionModeSelectedItem());
            }
        });

        projection_mode2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                global.setSystemSettingProjectionModeSelectedItem(1);
                setPictureModeImgVisibility(global.getSystemSettingProjectionModeSelectedItem());
            }
        });

        projection_mode3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                global.setSystemSettingProjectionModeSelectedItem(2);
                setPictureModeImgVisibility(global.getSystemSettingProjectionModeSelectedItem());
            }
        });

        projection_mode4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                global.setSystemSettingProjectionModeSelectedItem(3);
                setPictureModeImgVisibility(global.getSystemSettingProjectionModeSelectedItem());
            }
        });
    }

    private void viewFocus() {

        projection_mode1.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                settingLayout(projection_mode1, tv_projection_mode1, img_projection_mode1, hasFocus);
            }
        });

        projection_mode2.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                settingLayout(projection_mode2, tv_projection_mode2, img_projection_mode2, hasFocus);
            }
        });

        projection_mode3.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                settingLayout(projection_mode3, tv_projection_mode3, img_projection_mode3, hasFocus);
            }
        });

        projection_mode4.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                settingLayout(projection_mode4, tv_projection_mode4, img_projection_mode4, hasFocus);
            }
        });
    }

    private void settingLayout(ConstraintLayout constraintLayout, TextView title, ImageView icon, Boolean hasFocus) {
        if (hasFocus) {
            constraintLayout.setBackground(ContextCompat.getDrawable(Activity_ProjectionMode.this, R.drawable.bg_focus_tools));
            constraintLayout.setScaleX((float) 1.1);
            constraintLayout.setScaleY((float) 1.1);
            title.setTextColor(ContextCompat.getColor(context, R.color.blue0080CB));
            icon.setColorFilter(ContextCompat.getColor(context, R.color.blue0080CB));
        } else {
            constraintLayout.setBackground(ContextCompat.getDrawable(Activity_ProjectionMode.this, R.drawable.bg_solid_black_radius10));
            constraintLayout.setScaleX((float) 1);
            constraintLayout.setScaleY((float) 1);
            title.setTextColor(ContextCompat.getColor(context, R.color.whiteffffff));
            icon.setColorFilter(ContextCompat.getColor(context, R.color.whiteffffff));
        }
    }

    private void setPictureModeImgVisibility(int default_selected) {
        switch (default_selected){
            case 0:
                img_projection_mode1.setVisibility(View.VISIBLE);
                img_projection_mode2.setVisibility(View.GONE);
                img_projection_mode3.setVisibility(View.GONE);
                img_projection_mode4.setVisibility(View.GONE);
                break;
            case 1:
                img_projection_mode1.setVisibility(View.GONE);
                img_projection_mode2.setVisibility(View.VISIBLE);
                img_projection_mode3.setVisibility(View.GONE);
                img_projection_mode4.setVisibility(View.GONE);
                break;
            case 2:
                img_projection_mode1.setVisibility(View.GONE);
                img_projection_mode2.setVisibility(View.GONE);
                img_projection_mode3.setVisibility(View.VISIBLE);
                img_projection_mode4.setVisibility(View.GONE);
                break;

            case 3:
                img_projection_mode1.setVisibility(View.GONE);
                img_projection_mode2.setVisibility(View.GONE);
                img_projection_mode3.setVisibility(View.GONE);
                img_projection_mode4.setVisibility(View.VISIBLE);
                break;
        }

    }

}