package com.example.solidpro;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.util.DisplayMetrics;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import com.example.solidpro.RecyclerModel.ObjectFavApp;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.io.ByteArrayOutputStream;
import java.util.ArrayList;
import java.util.List;

import static android.content.Context.MODE_PRIVATE;

public class global {

    public static Context context;
    public static boolean isComeFormFav = false;
    public static int favSelectedPosition;
    public static String favSelectedAppPkgAddr;
    public static String favSelectedAppName;
    public static Drawable favSelectedImg;
    public static List<ObjectFavApp> favAppsList;
    public static List<Boolean> keystoneCorrectionDefaultSetting;
    public static boolean wifiConnectedResult;


    public static int windowWidth;
    public static int windowHeigh;


    public static void getWindowSize(Activity mactivity) {
        DisplayMetrics dm = new DisplayMetrics();
        mactivity.getWindowManager().getDefaultDisplay().getMetrics(dm);
        global.windowHeigh = dm.heightPixels;
        global.windowWidth = dm.widthPixels;
    }


    public static void setIsFirstTimeOpenApp(boolean mode) {
        SharedPreferences.Editor editor = ((AppCompatActivity) context).getPreferences(MODE_PRIVATE).edit();
        editor.putBoolean("isFirstTimeOpenApp", mode);
        editor.commit();
    }

    public static boolean getIsFirstTimeOpenApp() {
        return ((AppCompatActivity) context).getPreferences(MODE_PRIVATE).getBoolean("isFirstTimeOpenApp", true);
    }


    public static void initFavAppsList() {
        Drawable d = context.getDrawable(R.drawable.icon_add);
        Bitmap bitmap = ((BitmapDrawable) d).getBitmap();
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG, 100, stream);
        byte[] bitmapdata = stream.toByteArray();

        favAppsList = new ArrayList<>();
        favAppsList.add(0, new ObjectFavApp(bitmapdata, "", ""));
        favAppsList.add(1, new ObjectFavApp(bitmapdata, "", ""));
        favAppsList.add(2, new ObjectFavApp(bitmapdata, "", ""));
        favAppsList.add(3, new ObjectFavApp(bitmapdata, "", ""));
        favAppsList.add(4, new ObjectFavApp(bitmapdata, "", ""));
        favAppsList.add(5, new ObjectFavApp(bitmapdata, "", ""));

        SharedPreferences.Editor editor = ((AppCompatActivity) context).getPreferences(MODE_PRIVATE).edit();
        editor.putString("FavAppsList", new Gson().toJson(favAppsList));
        editor.apply();
        editor.commit();

    }

    public static void saveFavAppList() {
        if (favAppsList != null) {
            SharedPreferences.Editor editor = ((AppCompatActivity) context).getPreferences(MODE_PRIVATE).edit();
            editor.putString("FavAppsList", new Gson().toJson(favAppsList));
            editor.commit();
        }
    }


    public static void getFavAppsList() {

        SharedPreferences sp = ((AppCompatActivity) context).getPreferences(MODE_PRIVATE);
        String ListJson = sp.getString("FavAppsList", "Empty Array");

        if (!ListJson.isEmpty()) {
            Gson gson = new Gson();
            favAppsList = gson.fromJson(ListJson, new TypeToken<List<ObjectFavApp>>() {
            }.getType());
        }
    }

    public void replaceFragment(Activity mactivity, Fragment fragment) {
        FragmentManager fragmentManager = ((AppCompatActivity) mactivity).getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.frameContainer, fragment);
        fragmentTransaction.addToBackStack(fragment.toString());
        mactivity.getFragmentManager().popBackStack();
        fragmentTransaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
        fragmentTransaction.commit();
    }

    public void lunchReplaceFragment(Activity mactivity, Fragment fragment) {
        FragmentManager fragmentManager = ((AppCompatActivity) mactivity).getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.LunchSettingFrameContainer, fragment);
        fragmentTransaction.addToBackStack(fragment.toString());
        mactivity.getFragmentManager().popBackStack();
        fragmentTransaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
        fragmentTransaction.commit();
    }


    /* Init System setting default value*/
    public static void setSystemSettingPictureModeSelectedItem(int position){
        SharedPreferences.Editor editor = ((AppCompatActivity) context).getPreferences(MODE_PRIVATE).edit();
        editor.putInt("SystemSettingPictureModeSelectedItem", position);
        editor.apply();
        editor.commit();
    }

    public static int getSystemSettingPictureModeSelectedItem() {
        return ((AppCompatActivity) context).getPreferences(MODE_PRIVATE).getInt("SystemSettingPictureModeSelectedItem", 0);
    }


    public static void initSystemKeystoneCorrectionSetting() {
        keystoneCorrectionDefaultSetting = new ArrayList<Boolean>();
        keystoneCorrectionDefaultSetting.add(0,false);
        keystoneCorrectionDefaultSetting.add(1,false);
        SharedPreferences.Editor editor = ((AppCompatActivity) context).getPreferences(MODE_PRIVATE).edit();
        editor.putString("keystoneCorrectionDefaultSetting", new Gson().toJson(keystoneCorrectionDefaultSetting));
        editor.apply();
        editor.commit();
    }
    public static void saveKeystoneCorrectionSetting() {
        if (keystoneCorrectionDefaultSetting != null) {
            SharedPreferences.Editor editor = ((AppCompatActivity) context).getPreferences(MODE_PRIVATE).edit();
            editor.putString("keystoneCorrectionDefaultSetting", new Gson().toJson(keystoneCorrectionDefaultSetting));
            editor.apply();
            editor.commit();
        }
    }
    public static void getKeystoneCorrectionSetting() {
        SharedPreferences sp = ((AppCompatActivity) context).getPreferences(MODE_PRIVATE);
        String ListJson = sp.getString("keystoneCorrectionDefaultSetting", "Empty Array");

        if (!ListJson.isEmpty()) {
            Gson gson = new Gson();
            keystoneCorrectionDefaultSetting = gson.fromJson(ListJson, new TypeToken<List<Boolean>>() {
            }.getType());
        }
    }

    public static void setSystemSettingProjectionModeSelectedItem(int position){
        SharedPreferences.Editor editor = ((AppCompatActivity) context).getPreferences(MODE_PRIVATE).edit();
        editor.putInt("SystemSettingProjectionModeSelectedItem", position);
        editor.apply();
        editor.commit();
    }

    public static int getSystemSettingProjectionModeSelectedItem() {
        return ((AppCompatActivity) context).getPreferences(MODE_PRIVATE).getInt("SystemSettingProjectionModeSelectedItem", 0);
    }

    public static void setSystemSettingFocusModeGlobalZoom(int position){
        SharedPreferences.Editor editor = ((AppCompatActivity) context).getPreferences(MODE_PRIVATE).edit();
        editor.putInt("SystemSettingFocusModeGlobalZoom", position);
        editor.apply();
        editor.commit();
    }

    public static int getSystemSettingFocusModeGlobalZoom() {
        return ((AppCompatActivity) context).getPreferences(MODE_PRIVATE).getInt("SystemSettingFocusModeGlobalZoom", 0);
    }

    public static void setSystemSettingFocusModeVerticalZoom(int position){
        SharedPreferences.Editor editor = ((AppCompatActivity) context).getPreferences(MODE_PRIVATE).edit();
        editor.putInt("SystemSettingFocusModeVerticalZoom", position);
        editor.apply();
        editor.commit();
    }

    public static int getSystemSettingFocusModeVerticalZoom() {
        return ((AppCompatActivity) context).getPreferences(MODE_PRIVATE).getInt("SystemSettingFocusModeVerticalZoom", 0);
    }

    public static void setSystemSettingFocusModeHorizonZoom(int position){
        SharedPreferences.Editor editor = ((AppCompatActivity) context).getPreferences(MODE_PRIVATE).edit();
        editor.putInt("SystemSettingFocusModeHorizonZoom", position);
        editor.apply();
        editor.commit();
    }

    public static int getSystemSettingFocusModeHorizonZoom() {
        return ((AppCompatActivity) context).getPreferences(MODE_PRIVATE).getInt("SystemSettingFocusModeHorizonZoom", 0);
    }


}
